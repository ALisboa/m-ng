export const environment = {
  production: true,
  wasmAssetsPath: '/assets/wasm',
	socketurl: {url: 'http://localhost:9200', options:{}},
	DOMAIN_NAME: '192.168.1.6:8000',
};
