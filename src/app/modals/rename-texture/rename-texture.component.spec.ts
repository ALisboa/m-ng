import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenameTextureComponent } from './rename-texture.component';

describe('RenameTextureComponent', () => {
  let component: RenameTextureComponent;
  let fixture: ComponentFixture<RenameTextureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenameTextureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenameTextureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
