import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-rename-texture',
  templateUrl: './rename-texture.component.html',
  styleUrls: ['./rename-texture.component.scss']
})
export class RenameTextureComponent implements OnInit {

  name: string;
  model: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    console.log(data);
    this.model = data;
    this.name = data;
  }

  ngOnInit(): void {
  }

  acceptChanges(): void {

  }

  revertChanges(): void {

  }

}
