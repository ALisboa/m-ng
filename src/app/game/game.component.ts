import { Component, OnInit } from '@angular/core';
import { CharactersheetService } from '../services/charactersheet-service/charactersheet.service';
import { ChatServiceService } from '../services/chat/chat-service.service';
import { ContentService } from '../services/content/content.service';
import { DmEditorService } from '../services/dm-editor/dm-editor.service';
import { SynkrService } from '../services/synkr/synkr.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  constructor(
    private css: CharactersheetService,
    private cts: ContentService,
    private chat: ChatServiceService,
    private dms: DmEditorService,
    private synk: SynkrService,
  ) { }

  ngOnInit(): void {
    this.synk.InitLoadingDialog();
  }

}
