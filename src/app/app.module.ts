import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayComponent } from './grid/display/display.component';
import { OverlayComponent } from './grid/overlay/overlay.component';
import { GridContainerComponent } from './grid/grid-container/grid-container.component';
import { SideButtonsComponent } from './grid/side-buttons/side-buttons.component';
import { EditorComponent } from './grid/editor/editor.component';
import { EditorObjectComponent } from './component/editor-object/editor-object.component';
import { DmEditorComponent } from './component/dm-editor/dm-editor.component';
import { ConfigFormComponent } from './grid/config-form/config-form.component';
import { MatImportsModule } from 'src/app/mat-imports/mat-imports.module';
import { DmeCatComponent } from './component/dme-cat/dme-cat.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from '@angular/cdk/overlay';
import { ChatComponent } from './chat/chat.component';
import { MessageComponent } from './chat/message/message.component';
import {CharacterSheetComponent} from './character-sheet/character-sheet.component';
import { CharacterImageComponent } from './component/character-image/character-image.component';
import { CharactersheetService } from './services/charactersheet-service/charactersheet.service';
import { RenameTextureComponent } from './modals/rename-texture/rename-texture.component';
import { LoginModalComponent } from './login-modal/login-modal.component';
import { GameComponent } from './game/game.component';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    DisplayComponent,
    OverlayComponent,
    GridContainerComponent,
    SideButtonsComponent,
    EditorComponent,
    EditorObjectComponent,
    DmEditorComponent,
    ChatComponent,
    ConfigFormComponent,
    MessageComponent,
    DmeCatComponent,
    CharacterSheetComponent,
    CharacterImageComponent,
    RenameTextureComponent,
    LoginModalComponent,
    GameComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatImportsModule,
    BrowserAnimationsModule,
    OverlayModule
  ],
  providers: [
    CharactersheetService
  ],
  entryComponents: [
    ConfigFormComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
