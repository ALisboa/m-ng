interface Category {
  name: string,
  subCats: Array<Category>,
  items: Array<Texture>
}

interface Texture {
  name: string,
  id: number,
  preview: string,
}
export {Texture, Category}
