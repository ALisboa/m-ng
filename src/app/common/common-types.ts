/**
 * START; MESSAGE; ERROR; ACTION; ACTION_PAYLOAD; RIGHT_CLICK_MENU
 */

export enum ParserState {
  START = 0,
  WMSG = 1,
  LOG = 2,
  ERROR = 3,
  CUSTOM = 4,
  RIGHT_CLICK_MENU = 5,
}

export class CharacterSheet {
  id: any;
  rawContent: any;
  image: any;

  HP: any;
  name: any;
  classlvl: any;
  background: any;
  playerName: any;
  race: any;
  alignment: any;
  exp: any;
  inspiration: any;
  proficiency: any;

  maxHP: any;
  AC: any;
  initiative: any;
  speed: any;
  tempHP: any;
  hdTotal: any;
  hd: any;

  strSave: any;
  strSaveChk: any;
  conSave: any;
  conSaveChk: any;
  dexSave: any;
  dexSaveChk: any;
  wisSave: any;
  wisSaveChk: any;
  intSave: any;
  intSaveChk: any;
  chaSave: any;
  chaSaveChk: any;

  acrobatics: any;
  animal_handling: any;
  arcana: any;
  athletics: any;
  deception: any;
  history: any;
  insight: any;
  intimidation: any;
  investigation: any;
  medicine: any;
  nature: any;
  perception: any;
  performance: any;
  persuasion: any;
  religion: any;
  survival: any;
  sleight_of_hand: any;
  stealth: any;
  acrobaticsChk: any;
  animal_handlingChk: any;
  arcanaChk: any;
  athleticsChk: any;
  deceptionChk: any;
  historyChk: any;
  insightChk: any;
  intimidationChk: any;
  investigationChk: any;
  medicineChk: any;
  natureChk: any;
  perceptionChk: any;
  performanceChk: any;
  persuasionChk: any;
  religionChk: any;
  survivalChk: any;
  sleight_of_handChk: any;
  stealthChk: any;

  otherprofs: any;

  deathsuccess1: any;
  deathsuccess2: any;
  deathsuccess3: any;
  deathfail1: any;
  deathfail2: any;
  deathfail3: any;

  attack1 = {name: '', bonus: '', damage: ''};
  attack2 = {name: '', bonus: '', damage: ''};
  attack3 = {name: '', bonus: '', damage: ''};

  cp: any;
  sp: any;
  gp: any;
  pp: any;
  ep: any;

  equipment: any;

  personality: any;
  ideals: any;
  bonds: any;
  flaws: any;

  features: any;

  strength: any;
  get STR() {
    if(!this.strength){
      return null;
    } else {
      return Math.floor((this.strength-10)/2);
    }
  }
  constitution: any;
  get CON() {
    if(!this.constitution){
      return null;
    } else {
      return Math.floor((this.constitution-10)/2);
    }
  }
  dexterity: any;
  get DEX() {
    if(!this.dexterity){
      return null;
    } else {
      return Math.floor((this.dexterity-10)/2);
    }
  }
  wisdom: any;
  get WIS() {
    if(!this.wisdom){
      return null;
    } else {
      return Math.floor((this.wisdom-10)/2);
    }
  }
  intelligence: any;
  get INT() {
    if(!this.intelligence){
      return null;
    } else {
      return Math.floor((this.intelligence-10)/2);
    }
  }
  charisma: any;
  get CHA() {
    if(!this.charisma){
      return null;
    }else{
      return Math.floor((this.charisma-10)/2);
    }
  }

  constructor() {

  }
}


export class Image {
  name: any;
  data: any;
  id: any;
  roid: any;
}

export const DefaultProfileImage =  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAEe0lEQVR4Xu2b2yt8URTHv0gUUm7Jk6RILsnlQZEkCqFQ8ighkge5xIsHd+WBkkg88ErhDyBFaaR4xYuGKIoSUvxa+5f5zYyZObe995yZ+Z2aZubMOeusz2fvtaaZ0w6yWCzfMTExCAsLQyBtHx8feHp6QtD19fU3vUhNTUV0dHRAOHh+fsbl5SVo4IOsVut3REQE2xEIEn7gifX19fWvgKSkJNh/4K8zwZnx9vb2nwCa+/4swRXbLwH+KsHdwLoU4G8SPM1qtwL8RYJSSXsU4OsSlOCJT1GAr0pQA69agK9JUAuvSYCvSNACr1mA2SVohdclwKwS9MDrFmA2CXrhDQkwiwQj8IYFeFuCUXguArwlgQc8NwGyJfCC5ypAlgSe8NwFiJbAG16IAFESRMALE8Bbgih4oQJ4SRAJL1yAUQmi4aUI0CtBBrw0AVolyIKXKkCtBJnw0gUoSZAN7xUB7iR4A95rApwl0Htv3ZdU9a8wJShi+xl1iu2tm7L/BTjfHBUx0q5i2td8wJWAq4YXME3QE6g3JEjtAWoA1RzDs0ylCdACpuVYozKkCNADpOccPTKECzACYuRctTKECuABwCOGJxnCBPBMnGcsZxlCBIhIWERMIT+GRCWq9FNabc0LnQEi4X8S530NbiXAOzFPI8rzWlwE8ExI7VTmdU3DAnglohbc/jge1zYkgEcCesB5StAtwAzwPBqjLgFmgjcqQbMAM8IbkaBJgJnh9UpQLYAn/Pb2NgYGBvDw8ICMjAysrq6yZ/vt5OQERUVF2NjYQHNzs2KvtI+ZlpaG/v5+VFRUOKyBchVTlQCe8BcXFygrK8Pe3h7y8vIwOTmJm5sbLC8v2yDf399RWFiIl5cXTE1NKQpwFfPq6go9PT22v9vdxVQUwBOeCHt7exESEoK5uTm3o0qjFxwcjNPTU7S1tTEBKysrWFhYYPtCQ0OxtraGmZkZnJ2dYXBw0GVM+9zHxsZ+xVT8McQbni5YXFyMkpISHB8fg0YpKysL8/PzSElJYUKOjo7Q0dEBi8WC6upqmwD6jN7T+SSFztvd3UV+fr7HmMSwtbWF2dlZJss5pvQlM5mZmQgPD2fJx8XFsVo9PDxkI/v29sbKYn19nZVAeXm5g4C7uzu2n+DpeXR0lElTipmbm4vh4WHU1dWhoaHBIab0RVM0+pRIX18fS54WbMbGxuL+/h7j4+OIiooCTVfanAXQvu7ubtYvrFYrEhIS2HFqYpJouv9I1+3s7LT1FenL5lpbWxEfH4/p6WmW/OPjI5sJJIJGlmqfHrSRlMjISLS3tzM51OyqqqpQU1PDGuTm5iY7Tm3Mr68vFpMkU5lRTAcBImreudNR7dfW1mJ/fx/p6emsgVHd08N5s58Bn5+fKCgowNDQEOrr65GdnY2JiQk0NjayfqI2ZmlpKSorK9HV1cW+Im0CZC6dXVxcZF9/VPPUxJaWlpCcnOxRwMjICJsBOzs77LiDgwM0NTXh/PwciYmJ0BKzpaUFOTk57CuSLZ0N+MXTgb58/g/yoZ5M8GsZVQAAAABJRU5ErkJggg=='
