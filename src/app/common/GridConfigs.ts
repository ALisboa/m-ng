export interface GridConfigs {
    color: number;
    colorAlpha: number;
    width: number;
    backgroundColor: number;
    backgroundColorAlpha: number;
    tilesize_x: number;
    tilesize_y: number;
    gridsize_x: number;
    gridsize_y: number;
  }
  
