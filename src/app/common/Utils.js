//BY Kaizhu256, https://gist.github.com/kaizhu256/4482069

export let UUID4 = function () {
    //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    var uuid = '', ii;
    for (ii = 0; ii < 32; ii += 1) {
      switch (ii) {
      case 8:
      case 20:
        uuid += '-';
        uuid += (Math.random() * 16 | 0).toString(16);
        break;
      case 12:
        uuid += '-';
        uuid += '4';
        break;
      case 16:
        uuid += '-';
        uuid += (Math.random() * 4 | 8).toString(16);
        break;
      default:
        uuid += (Math.random() * 16 | 0).toString(16);
      }
    }
    return uuid;
  };



export const UserDB = [
  { username: 'dmaster1', id: '50012', password: 'password1', Rol: 0 },
  { username: 'player1', id: '51242', password: 'password1', Rol: 1 },
  { username: 'player2', id: '51244', password: 'password1', Rol: 1 },
  { username: 'player3', id: '51245', password: 'password1', Rol: 1 },
  { username: 'player4', id: '51247', password: 'password1', Rol: 1 },
]
