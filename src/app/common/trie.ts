class TrieNode {
    value: any | undefined;
    children: Map<string, TrieNode> = new Map();
    constructor(){
    }
}

function Find(term: string, tree: TrieNode): any | null {
    const node = _getNode(tree, term);
    if(node){
        return node.value;
    }
    return null;
}

function Insert(term: string, value: any, tree: TrieNode) {
    for ( var char of term ){
        if(!tree){
            console.warn("Tree Fuking undefined")
        }
        if ( tree.children.has( char ) == false ){
            tree.children.set( char, new TrieNode() );
        }
        tree = tree.children.get( char );
    }
    tree.value = value;
}

function KeysWithPrefix( root: TrieNode, prefix: string ): string[] {
    let result: Array<string> = new Array();
    let nodes = _getNode(root, prefix);
    _collect( nodes, [prefix], result );
    return result;
}

function _getNode(root: TrieNode, prefix: string): TrieNode | undefined {
    for(var char of prefix){
        if ( root.children.has( char ) ){
            root = root.children.get( char );
        } else {
            return undefined;
        }
    }
    return root;
}

function _collect( node: TrieNode | undefined, prefix: string[], result: Array<string> ){
    if( !node ){
        return;
    }
    if( node.value ) {
        let prefix_str = ''.concat(...prefix);
        result.push(prefix_str);
    }
    for( var char of node.children.keys() ){
        prefix.push( char );
        _collect( node.children.get(char), prefix, result );
        prefix.pop();
    }
}

function Delete(root: TrieNode, key: string): boolean {
    return _delete(root, key, 0);
}

function _delete(node: TrieNode, key: string, d: number): boolean {
    if ( d == key.length ) {
        node.value = undefined;
    } else {
        let c = key[d];
        if ( node.children.has( c ) && _delete(node.children[c], key, d + 1)){
            node.children.delete( c );
        }
    }
    return !(node.value) && ( node.children.size === 0 );
}

export {TrieNode, KeysWithPrefix, Find, Insert, Delete}
