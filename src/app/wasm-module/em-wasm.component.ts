import { AfterViewInit, Component } from '@angular/core';
import { loadScript } from './tools';
import { environment } from '../../environments/environment';
import { EmModule } from './types';

@Component({
  selector: 'app-em-wasm-base',
  template: '<a></a>'
})
export abstract class EmWasmComponent implements AfterViewInit {

  ALLOC_NORMAL = 0; // Tries to use _malloc()
  ALLOC_STACK = 1; // Lives for the duration of the current function call
  ALLOC_DYNAMIC = 2; // Cannot be freed except through sbrk
  ALLOC_NONE = 3; // Do not allocate

  protected module: any;
  private moduleId: string;
  private wasmJavaScriptLoader: string;
  private moduleFactory: (mod: EmModule) => EmModule;

  protected setupWasm(moduleId: string, wasmJavaScriptLoader: string, moduleFactory: (mod: EmModule) => EmModule) {
    this.moduleId = moduleId;
    this.wasmJavaScriptLoader = wasmJavaScriptLoader;
    this.moduleFactory = moduleFactory;
  }

  ngAfterViewInit(): void {
    loadScript(this.moduleId, `${environment.wasmAssetsPath}/${this.wasmJavaScriptLoader}`)
      .subscribe(() => {
        const baseModule = {
          locateFile: (file: string) => `${environment.wasmAssetsPath}/${file}`
        } as EmModule;
        this.module = window[this.moduleId](this.moduleFactory(baseModule));
      });
  }


  /**
   * Creates a danew data file in the memory
   * @param fileName the file name
   * @param inputArray the file contents
   */
  protected createDataFile(fileName: string, inputArray: Uint8Array, canRead?: boolean, canWrite?: boolean) {
    try {
      this.module.FS_createDataFile('/', fileName, inputArray, canRead, canWrite);
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err;
      }
    }
  }

  /**
   * Reads a file from the memory as a text
   * @param fileName the file name
   */
  protected readTextFile(fileName: string): string {
    return this.module.FS_readFile(fileName, { encoding: 'utf8' });
  }
}
