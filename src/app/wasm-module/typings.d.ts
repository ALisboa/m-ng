/**
 * This is a simple typings definition for Emscripten's Module object
 */
export interface EmReadFileOptions {
  encoding?: 'utf8' | 'binary';
  flags?: string;
}

export interface CcallOptions {
  async?: boolean;
}

export interface EmModule {
  arguments?: string[];
  preRun?: () => void[];
  postRun?: () => void[];
  canvas?: HTMLCanvasElement;
  keyboardListeningElement?: HTMLElement;
  print?(what: string): void;
  printErr?(what: string): void;
  locateFile?(file: string): string;
  ccall?(funcName: string, returnType: string, argumentTypes: string[], arguments: any[], options?: CcallOptions): any;
  FS_createDataFile?(parent: string, name: string,
                     data: string | Uint8Array, canRead?: boolean, canWrite?: boolean,
                     canOwn?: boolean): void;
  FS_createPreloadedFile?(parent: string, name: string, url: string, canRead?: boolean, canWrite?: boolean): void;
  FS_readFile?(url: string, options?: EmReadFileOptions): any;
  FS_unlink?(path: string): void;
  addFunction?(f: any, t?: string): number;
	allocate?(what: any, t: string, allocation_method: MemoryAllocation): number;
	allocateUTF8(str: any): any;
	_free(ptr: any): any;
}

export declare enum MemoryAllocation{
ALLOC_NORMAL = 0, // Tries to use _malloc()
ALLOC_STACK = 1, // Lives for the duration of the current function call
ALLOC_DYNAMIC = 2, // Cannot be freed except through sbrk
ALLOC_NONE = 3, // Do not allocate
}
