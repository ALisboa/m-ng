import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game/game.component';
import { LoginModalComponent } from './login-modal/login-modal.component';

const routes: Routes = [
  {path: 'game', component: GameComponent},
  {path: '', component: LoginModalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
