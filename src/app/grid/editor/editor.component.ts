import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CharacterSheetComponent } from 'src/app/character-sheet/character-sheet.component';
import { EObject } from 'src/app/common/eobject';
import { CharactersheetService } from 'src/app/services/charactersheet-service/charactersheet.service';
import { ContentService } from 'src/app/services/content/content.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  @ViewChild('Container') container: ElementRef;
  objects: Array<EObject>;
  selectedObject: any;
  editing: boolean = false;
  old: any;

  constructor(
    public _contentService: ContentService,
    private _characterSheets: CharactersheetService,
    private mdialog: MatDialog,
  ) {

  }

  ngOnInit() {
  }

  HideAll(){
    let ne = <HTMLElement>this.container.nativeElement;
    const children = Array.from(ne.getElementsByTagName('div'));
    children.forEach(child => {
      child.setAttribute("style", "display: none;");
    });
  }

  Open(id: any){
    this._contentService.content.forEach(obj => {
      if(obj.id === id){
        this.selectedObject = obj;
      }
    });
  }

  Content() {
    return this._contentService.content ?? [];
  }

  Characters() {
    return this._characterSheets.characters ?? [];
  }

  public newCharacter() {
    this._characterSheets.CreateSheet();
  }

  public newPage() {
    this._contentService.newPage();
  }

  ShowCS(cs) {
    const dialogRef = this.mdialog.open(CharacterSheetComponent, {
      height: '100%',
      data: cs
    });
  }

  DeleteSelectedObject() {
    if(this.isCS()){
      this._characterSheets.DeleteCS(this.selectedObject);
    } else {
      this._contentService.RemoveContent(this.selectedObject);
    }
    this.ClearSelection();
  }

  ClearSelection() {
    this.selectedObject = null;
    this.editing = false;
    this.old =  null;
  }

  Save() {
    console.log(this.selectedObject);
    this.editing = false;
    this.old = null;

    //TODO: Send to synk service;
    this._contentService.UpdateContent(this.selectedObject);
  }

  RevertChanges() {
    if(this.selectedObject && this.old){
      if(this.old.title){
        this.selectedObject.title = this.old.title;
      }
      this.selectedObject.rawContent = this.old.content;
      this.editing = false;
    }
  }

  EditSelectedObject() {
    if(!this.selectedObject) {
      return;
    }
    this.editing = true;
    this.old = {
      title: this.selectedObject.title,
      content: this.selectedObject.rawContent,
    }
  }

  Select(obj: any) {
    if(obj){
      this.editing = false;
      this.selectedObject = obj;
      this.old = null;
    }
  }

  CanEdit(): boolean {
    return (this.selectedObject) ? true : false;
  }

  CanRemove(): boolean {
    return (this.selectedObject) ? true : false;
  }

  isCS(): boolean {
    return this.selectedObject && !this.selectedObject.title;
  }
}
