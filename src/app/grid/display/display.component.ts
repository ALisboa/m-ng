import { Component, OnInit, HostListener, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { EmWasmComponent } from 'src/app/wasm-module/em-wasm.component';
import normalizeWheel from './Wheel.js';
import { Subscription } from 'rxjs';
import { WasmService } from 'src/app/services/wasm/wasm.service';
import { DmEditorService } from 'src/app/services/dm-editor/dm-editor.service';
import ImageTools from 'src/app/common/ImageTools.js';
import { GridConfigs } from 'src/app/common/GridConfigs.js';
import {UserService} from 'src/app/services/user/user.service';
import {SynkrService, MessageAction} from 'src/app/services/synkr/synkr.service';
import { EmModule } from 'src/app/wasm-module/types';
import { ParserState } from 'src/app/common/common-types';
import { CharactersheetService } from 'src/app/services/charactersheet-service/charactersheet.service.js';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent extends EmWasmComponent implements OnDestroy{
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('secret') sizer: ElementRef;
  @ViewChild('wrapper') wrapper: ElementRef;
  imageUploadedEvent: Subscription;
  changeStateEvent: Subscription;
  layerChanged: Subscription;
  synkerNewImage: Subscription;
  synkerNewUpdate: Subscription;
  makeTokenCalled: Subscription;
  changeToLayerCalled: Subscription;
  removeRoCalled: Subscription;
  copyRoCalled: Subscription;
  pasteRoCalled: Subscription;
  ggs: GridConfigs;
  ps = ParserState.LOG;
  ParserCallback: (out: string) => void;
  gridModule: EmModule;
  MousePosition = {x: 0, y: 0};
  @HostListener('document:mousemove', ['$event']) mouseMove( event: MouseEvent ) {
    this.MousePosition.x = event.screenX;
    this.MousePosition.y = event.screenY;
  }


  constructor(private ngZone: NgZone,
              private wasmService: WasmService,
              private deditor: DmEditorService,
              public users: UserService,
              private synkr: SynkrService,
             ) {
    super();
    this.ggs = {
      color: 0x2D2D55,
      colorAlpha: 0x2D,
      backgroundColor: 0xFFFFFF,
      backgroundColorAlpha: 0xFF,
      tilesize_x: 64,
      tilesize_y: 64,
      gridsize_x: 16,
      gridsize_y: 16,
      width: 2
    };
    this.wasmService.SetComponent(this);
    this.setupWasm(
      'GridModule',
      'grid.js',
      mod => Object.assign(mod, {
        canvas: this.canvas.nativeElement as HTMLCanvasElement,
        printErr: (what: string) => {
          console.info(what);
        },
        print: (stdout: string) => {
          this.ngZone.runTask(() => {
            this.parseWasmOutput(stdout);
          });
        },
        postRun: [
          () => {
            this.module.then( ( loadedModule: EmModule ) =>
              {
                this.gridModule = loadedModule;
                this.SetParserFunct();
                this.SpectMode();
                this.synkr.connect(this.users.currentRoom);
              }
                            );
          }
        ],
        doNotCaptureKeyboard: true,
      })
    );
    this.AddObservers();
  }

  test() {
    {	// Callback Test
      const cb = (out: string) => {
        console.log(out);
      };
      this.GetLayerStatus(cb);
    }

  }

  private AddObservers(){
    this.imageUploadedEvent = this.wasmService.uploadedFile.subscribe( (a) => {
      if (a) {
        this.OnFileUploaded(a);
      }
    });
    this.changeStateEvent =
      this.wasmService.changeState.subscribe((mesage) => {
        if (mesage) {
          this.ChangeWasmState(mesage as number);
        }
      }) ;
    this.layerChanged = this.wasmService.layerChanged.subscribe((id) => {
      this.ChangeLayer(id as number);
    });
    this.synkerNewImage = this.synkr.new_image.subscribe((msg) => {
      this.synkr_new_image(msg);
    });
    this.synkerNewUpdate = this.synkr.update.subscribe((msg) => {
      this.newWsmUpdate(msg);
    });
    this.makeTokenCalled = this.wasmService.$MakeToken.subscribe( (id: {id: number}) => this.MakeToken(id) );
    this.changeToLayerCalled = this.wasmService.$ChangeRolayer.subscribe(
      (res: {id: number, layerId: number}) => {
        this.ChangeRoLayer(res);
      });
    this.removeRoCalled = this.wasmService.$RemoveRo.subscribe(
      (res: {id: number}) => {
        this.RemoveRo(res.id);
      }
    );
    this.copyRoCalled = this.wasmService.$CopyRoToCP.subscribe(
      (res: any) => {
        this.CopySelectedRo();
      }
    );
    this.pasteRoCalled = this.wasmService.$PasteRo.subscribe(
      (res: any) => {
        this.PasteRoFromClipboard();
      }
    );

  }



  ToggleVisionMode(){
    this.gridModule.ccall('ToggleVisionMode', 'void', [], []);
  }

  SetParserFunct(){
    const f = this.gridModule.addFunction(() => {
      this.ngZone.run(
        () => {
          this.RestartWasmParser();
        }
      );
    }, 'v');
    this.gridModule.ccall('SetRestartParser', 'void', ['number'], [f]);
  }

  SpectMode(){
    if (!this.users.isDM()) {
      const r = this.gridModule.ccall('ToggleSpectator', 'int', [], []);
    }
    else {
      this.gridModule.ccall('GenGM', 'void', [], []);
      const r = this.gridModule.ccall('SwitchUser', 'int', ['int'], [1]);
      if (r !== 1) {
        throw new Error("Couln't change into default GM");
      }
    }
  }

  ngOnDestroy(): void {
    this.imageUploadedEvent.unsubscribe();
    this.changeStateEvent.unsubscribe();
    this.makeTokenCalled.unsubscribe();
    this.layerChanged.unsubscribe();
    this.synkerNewImage.unsubscribe();
    this.synkerNewUpdate.unsubscribe();
    this.removeRoCalled.unsubscribe();
    this.copyRoCalled.unsubscribe();
    this.pasteRoCalled.unsubscribe();
    this.gridModule.ccall('Q', 'void', [], []);
  }

  LoadTexture(file: File) {
    const reader = new FileReader();
    const filename = (file.name) ? file.name : 'noname';
    if (file.type === 'image/jpeg' || file.type === 'image/jpg') {
      file = ImageTools.ConvertToPNG(file);
    }
    reader.onloadend = () => {
      const inputArray = new Uint8Array(reader.result as ArrayBuffer);
      const id = this.AddImage(filename, inputArray);
      this.deditor.GeneratePreview(file, filename, id);
      return id;
    };
  }

  OnFileUploaded(files: any, nosynk?: boolean, _id?: number){
    if (!files) {
      return;
    }
    let file = files[0];
    if(!file) {
      return;
    }
    let filename = (file.name) ? file.name : Math.random().toString();
    let reader = new FileReader();
    //console.log(file.type);
    if (file.type === 'image/jpeg' || file.type === 'image/jpg') {
      file = ImageTools.ConvertToPNG(file);
    }
    reader.onloadend = () => {
      let inputArray = new Uint8Array(reader.result as ArrayBuffer);
      if(typeof _id === 'undefined')
        _id = this.AddImage(filename, inputArray);
      else {
        this.InsertImage(filename, inputArray, _id);
      }
      this.deditor.GeneratePreview(file, filename, _id);
      if (!nosynk) {
        reader.onloadend = () => {
          // console.log(reader.result);
          this.synkr.sendMessage({
            action: MessageAction.NEW_IMAGE ,
            payload: { 'filename': filename, 'inputArray': reader.result, 'id': _id},
            id : '0'
          });
        };
        reader.readAsDataURL(file);
      }
    };
    reader.readAsArrayBuffer(file);
  }

  AddImage(filename: string, inputArray: Uint8Array): number{
    this.gridModule.FS_createDataFile('/', filename, inputArray, true);
    //TODO: SANITY CHECKS
    const result = this.gridModule.ccall('AddImage', 'int', ['string'], [filename]);
    if (result > 0) {
      //const ROId = this.gridModule.ccall('AddRo', 'int', ['int'], [result]);
      //if (ROId < 1) {
      //  console.log('ERROR: RO CAN\'T BE CREATED');
      //}
    }
    //this.gridModule.FS_unlink(filename);
    return result;
  }

  InsertImage(filename: string, inputArray: Uint8Array, id: number): number {
    this.gridModule.FS_createDataFile('/', filename, inputArray, true);
    //TODO: SANITY CHECKS
    const result = this.gridModule.ccall('InsertImage', 'int', ['string', 'int'], [filename, id]);
    if(result < 0) {
      console.error("LOG ERROR INSERTING IMAGE")
    }
    return result;
  }

  StringToIntArray(str: string){
    const array = [];
    for(let i = 0; i < str.length; i++){
      array.push(str.charCodeAt(i));
    }
    return array;
  }

  StringToPointer(str: string): number{
    return this.gridModule.allocate(this.StringToIntArray(str), 'i8', 0);
  }

  resizeToFitSizer(){
    const sizer = this.sizer.nativeElement as HTMLDivElement;
    this.gridModule.ccall('ResizeWindow', 'void', ['int', 'int'], [sizer.clientWidth, sizer.clientHeight]);
  }

  ChangeWasmState(i: number){
    this.gridModule.ccall('SetState', 'void', ['int'], [i]);
  }

  printCanvasInfo(){
  }

  scroll(e: WheelEvent){
    let nw = normalizeWheel(e);
    this.wrapper.nativeElement.scroll(
      this.wrapper.nativeElement.scrollLeft + nw.pixelX,
      this.wrapper.nativeElement.scrollTop + nw.pixelY)
  }

  private SetGridColor(color: number, alpha: number) {
    /* tslint:disable:no-bitwise */
    const c = color + (alpha << 24);
    /* tslint:enable:no-bitwise */
    this.gridModule.ccall('SetGridColor', 'void', ['int'], [c]);
    this.ggs.color = color;
    this.ggs.colorAlpha = alpha;
  }

  private SetBackgroundColor(color: number, alpha: number){
    /* tslint:disable:no-bitwise */
    const c = color + (alpha << 24);
    /* tslint:enable:no-bitwise */
    this.gridModule.ccall('SetGridBackgroundColor', 'void', ['int'], [c]);
    this.ggs.backgroundColor = color;
    this.ggs.backgroundColorAlpha = alpha;
  }

  private SetTileSize(sizex: number, sizey: number){
    this.gridModule.ccall('SetTileSize', 'void', ['int', 'int'], [sizex, sizey]);
    this.ggs.tilesize_x = sizex;
    this.ggs.tilesize_y = sizey;
  }

  private SetGridSize(sizex: number, sizey: number){
    this.gridModule.ccall("SetGridSize", "void", ["int", "int"], [sizex, sizey])
    this.ggs.gridsize_x = sizex;
    this.ggs.gridsize_y = sizey;
  }

  private SetGridWitdh(size: number){
    this.gridModule.ccall('SetGridWidth', 'void', ['int'], [size]);
    this.ggs.width = size;
  }

  /**
   * getCurrentGridConfigsigs
   */
  public getCurrentGridConfigs(): GridConfigs {
    return this.ggs;
  }


  /**
   * setGridConfigs
   */
  public setGridConfigs(gcs: GridConfigs) {
    if ( gcs.color !== this.ggs.color || gcs.colorAlpha !== this.ggs.colorAlpha) {
      this.SetGridColor(gcs.color , gcs.colorAlpha);
    }
    if ( gcs.backgroundColor !== this.ggs.backgroundColor ||  gcs.backgroundColorAlpha !== this.ggs.backgroundColorAlpha) {
      this.SetBackgroundColor(gcs.backgroundColor, gcs.backgroundColorAlpha);
    }
    if ( gcs.gridsize_x !== this.ggs.gridsize_x || gcs.gridsize_y !== this.ggs.gridsize_y) {
      this.SetGridSize(gcs.gridsize_x, gcs.gridsize_y);
    }
    if ( gcs.tilesize_x !== this.ggs.tilesize_x || gcs.tilesize_y !== this.ggs.tilesize_y) {
      this.SetTileSize(gcs.tilesize_x, gcs.tilesize_y);
    }
    if ( gcs.width !== this.ggs.width) {
      this.SetGridWitdh(gcs.width);
    }
  }

  /**
   * Adds a new Render Object Using the ID of a Texture
   * @param id Texture ID
   */
  private NewRo(id: number) {
    // TODO: Add Error Catching
    //console.log(
    this.gridModule.ccall('AddRo', 'int', ['int'], [id]);
    //);
  }

  private NewRoIn(id: number, x: number, y: number) {
    this.gridModule.ccall('AddRoIn', 'int', ['int', 'int', 'int'], [id, x, y]);
  }

  public DropHandler(event){
    console.log(event);
    const a  = JSON.parse(event.dataTransfer.getData('text/json'));
    if (a.et === 'texture') {
      //this.NewRo(a.id);
      this.NewRoIn(a.id, event.layerX, event.layerY);
    }
    event.preventDefault();
  }

  public DragOverHandle(event){
    event.preventDefault();
  }

  public EmptyHandler(event){
    event.preventDefault();
  }

  ChangeLayer(id: number){
    // TODO: CHANGE THIS TO CHECK FOR THE TYPE OF LAYER
    if (id === 4){
      this.ToggleVisionMode();
      this.wasmService.SetDrawMode();
    }
    else{
      this.gridModule.ccall('SwitchLayerByID', 'int', ['int'], [id]);
    }
  }

  /**
   * Entry point of the Automata that parses the Cout of the Wasm Module
   */
  public parseWasmOutput(out: string){
    switch (this.ps) {
      case ParserState.START:
        this.ChangeParserState(out);
        break;
      case ParserState.WMSG:
        this.WasmMessage(out);
        break;
      case ParserState.LOG:
        this.ParseMessage(out);
        break;
      case ParserState.ERROR:
        this.ParseError(out);
        break;
      case ParserState.CUSTOM:
        this.ParserCustom(out);
        break;
      case ParserState.RIGHT_CLICK_MENU:
        this.ParserRightClick(out);
        break;
      default:
        break;
    }
  }

  /**
   * Start Broadcast of the message
   */
  WasmMessage(msg: string){
    this.synkr.sendMessage({action: MessageAction.UPDATE, payload: msg, id: '0'});
  }

  /**
   * Forces the automata to it's initial state
   */
  public RestartWasmParser(){
    this.ps = ParserState.START;
  }

  /**
   * Changes the State of the parser Automata
   */
  private ChangeParserState(out: string){
    try {
      const e: number = JSON.parse(out);
      this.ps = e;
    } catch (err) {
      console.error(err);
      this.ps = ParserState.START;
    }
  }

  /**
   * Loggin a Message from the WASM module
   */
  private ParseMessage(out: string){
    console.log(out);
  }

  /**
   * Logging an Error from the Wasm Module
   */
  private ParseError(out: string){
    console.error(out);
  }

  /**
   * Send the message to a custom callback
   * Ussage is SetCallback
   * Call Wasm Function: Long String
   * Take the Callback
   * Remove Callback
   * Reset Automata state
   */
  private ParserCustom(out: string){
    this.ParserCallback(out);
  }

  /**
   * Sends the event to create a new context menu on the last recorded mouse position
   */
  private ParserRightClick(out: string){
    console.log(out);
    const ojb = JSON.parse(out);
    this.wasmService.OpenContextMenu(this.MousePosition.x, this.MousePosition.y, ojb);
  }

  private synkr_new_image(payload: any){
    const p = payload.payload;
    fetch(p.inputArray).then(res => res.blob()).then(blob => {
      this.OnFileUploaded([blob], true, p.id);
    });
  }

  private newWsmUpdate(msg: any){
    const update = msg.payload;
    let res = 0;
    const buf = this.gridModule.allocateUTF8(update);
    res = this.gridModule.ccall('SerializedUpdate', 'int', ['i'], [buf]);
    this.gridModule._free(buf);
    if (res !== 1) {
      console.error('Error reading Update');
    }
  }

  public GetLayerStatus(callback: (out: string) => void): any{
    this.ps = ParserState.CUSTOM;
    this.ParserCallback = callback;
    this.gridModule.ccall('PrintCurrentLayers', 'void', ['void'], []);
  }

  public ChangeBrush(color: number, width: number){
    this.gridModule.ccall('ChangeLineConfig', 'void', ['int', 'int'], [color, width]);
  }

  public StoreMousePosition(event: MouseEvent){
    this.MousePosition = {x: event.screenX, y: event.screenY};
  }

  public MakeToken({id}: {id: number}): void {
    this.gridModule.ccall('MakeToken', 'void', ['int'], [id]);
  }

  public RemoveRo(id: number){
    this.gridModule.ccall('RemoveRo', 'void', ['int'], [id]);
  }

  public ChangeRoLayer({ id, layerId }: { id: number; layerId: number; }){
    this.gridModule.ccall('ChangeRoLayer', 'void', ['int', 'int'], [id, layerId]);
  }

  public RemoveSelectedRo() {
    this.gridModule.ccall('RemoveSelectedRo', 'void', [], []);
  }

  public CopySelectedRo() {
    this.gridModule.ccall('CopySelected', 'void', [], []);
  }

  public PasteRoFromClipboard() {
    this.gridModule.ccall('PasteFromClipboard', 'void', [], []);
  }

  public KeyUp(event: any) {
    console.log(event);
  }
}
