import { Component, OnInit } from '@angular/core';
import { OverlayService } from 'src/app/services/overlay/overlay.service';
import { DisplayPage } from 'src/app/common/DisplayPage';
import { Subscription } from 'rxjs';
import {UserService} from 'src/app/services/user/user.service';


@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss']
})
export class OverlayComponent implements OnInit {

  private displaying: DisplayPage;
  private TabAction: Subscription;

  constructor( private _OverlayService: OverlayService,
		public users: UserService
	) { 
    this.displaying = DisplayPage.Editor;
    this.TabAction = this._OverlayService?.TabAction?.subscribe((tab) => {this.displayTab(tab)})
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.TabAction.unsubscribe();
  }

  displayTab(tab:DisplayPage){
    this.displaying = tab;
  }

  toggleRightBar(){
    this._OverlayService.toggleRightMenu();
  }

  DisplayingEditor():boolean{
    return this.users.isDM() && this.displaying === DisplayPage.Editor;
  }

  DisplayingChat():boolean {
    return this.displaying === DisplayPage.Chat;
  }

  DisplayingJournal():boolean {
    return this.displaying === DisplayPage.Journal;
  }

  DisplayingRightMenu():boolean {
    return this._OverlayService.RightMenuOpen;
  }
  

  /*
   * 1 = JOURNAL
   * 2 = CHAT
   * 3 = EDITOR
   *
   */ 
  OpenTab(n:number){
    switch(n){
      case 1:{
        this.displaying = DisplayPage.Journal
        break;
      }
      case 2:{
        this.displaying = DisplayPage.Chat;
        break;
      }
      case 3:{
				if(this.users.isDM())
        this.displaying = DisplayPage.Editor;
        break;
      }
      default: {
        console.log("ERROR?");
        break;
      }
    }
  }

}
