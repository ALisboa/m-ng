import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GridConfigs } from 'src/app/common/GridConfigs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-config-form',
  templateUrl: './config-form.component.html',
  styleUrls: ['./config-form.component.scss']
})
export class ConfigFormComponent{
  configs: FormGroup;
  configData: GridConfigs;
  viewcolor: string;
  viewBGColor: string;

  constructor(
    public dialogRef: MatDialogRef<ConfigFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: GridConfigs,
  ) {
    this.configData = data;
    this.viewcolor = this.RGBtoHex(this.data.color);
    this.viewBGColor = this.RGBtoHex(this.data.backgroundColor);
  }

  SaveForm(): void{
    this.configData.color = parseInt(this.viewcolor.slice(1, 7), 16);
    this.configData.backgroundColor = parseInt(this.viewBGColor.slice(1, 7), 16);
  }
  OnNoClick(): void {
    this.dialogRef.close();
  }

  SaveClick(): void {
    this.SaveForm();
    this.dialogRef.close(
      this.configData
    );
  }

  ColorToHex(c: number ): string{
    let h = c.toString(16);
    if(h.length < 2){
      h = "0" + h;
    }
    return h;
  }

  RGBtoHex(rgb: number): string {
    let h = "#";
    h += this.ColorToHex((rgb & 0xFF0000) >> 16);
    h += this.ColorToHex((rgb & 0x00FF00) >> 8);
    h += this.ColorToHex((rgb & 0x0000FF));
    return h;
  }
}
