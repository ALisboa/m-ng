import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridContainerComponent } from './grid-container.component';
import { DisplayComponent } from '../display/display.component';
import { OverlayComponent } from '../overlay/overlay.component';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

describe('GridContainerComponent', () => {
  let component: GridContainerComponent;
  let fixture: ComponentFixture<GridContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        GridContainerComponent,
        DisplayComponent,
        OverlayComponent,
       ],
      imports: [
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {} },
        { provide: UserService, useValue: {} }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(1).toBeTruthy();
  });
});
