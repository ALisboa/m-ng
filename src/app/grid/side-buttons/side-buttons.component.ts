import { Component, HostListener, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { OverlayService } from 'src/app/services/overlay/overlay.service';
import { WasmService } from 'src/app/services/wasm/wasm.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfigFormComponent } from 'src/app/grid/config-form/config-form.component';
import {UserService} from 'src/app/services/user/user.service';
import { Subscription } from 'rxjs';
import { OverlayRef } from '@angular/cdk/overlay';
import { ViewContainerRef } from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { filter, take } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { TemplatePortal } from '@angular/cdk/portal';

enum MouseMode {
  SelectionMode, DrawMode
}

@Component({
  selector: 'app-side-buttons',
  templateUrl: './side-buttons.component.html',
  styleUrls: ['./side-buttons.component.scss']
})
export class SideButtonsComponent implements OnInit {
  BrushOverlayOpen = false;
  private _BrushColor = "#000000";
  private _BrushAlpha = 255;
  private _BrushWidth = 3;
  private overlayRef: OverlayRef | null;
  private ContextMenuOutsideClickSubscription: Subscription;
  private RightClickMenuSubscription: Subscription;
  @ViewChild('ContextMenu') contextMenu: TemplateRef<any>;
  @HostListener('document:click', ['$event']) OnGlobalClick( event: MouseEvent ) {
    const target = event.target as HTMLElement;
    if( !!this.overlayRef && !this.overlayRef.overlayElement.contains(target) ){
      this.CloseContextMenu();
    }
  }


  constructor (
    public _WasmService: WasmService,
    private _OverlayService: OverlayService,
    private mdialog: MatDialog,
    public users: UserService,
    public overlay: Overlay,
    public viewContainerRef: ViewContainerRef,
  ) {
    this.RightClickMenuSubscription = this._WasmService.RightClickCalled.subscribe((next: { x: number; y: number; ctx: any; }) => {
      this.ContextMenu(next);
    });
  }

  public get BrushWidth(): number {
    return this._BrushWidth;
  }

  public set BrushWidth(value: number) {
    if (0 > value && value < 100) {
      this._BrushWidth = value;
      this.SendBrushUpdate();
    }
  }

  public get BrushColor(): any {
    return this._BrushColor;
  }

  public set BrushColor(value) {
    this._BrushColor = value;
    this.SendBrushUpdate();
  }

  public get BrushAlpha(): number {
    return this._BrushAlpha;
  }

  public set BrushAlpha(value) {
    this._BrushAlpha = value;
    this.SendBrushUpdate();
  }

  ngOnInit() {
  }

  ShowingRightButtons(): boolean {
    return this._OverlayService.RightMenuOpen;
  }

  OpenEditor() {
    this._OverlayService.OpenEditor();
  }

  OpenJournal(){
    this._OverlayService.OpenJournal();
  }

  OpenChat() {
    this._OverlayService.OpenChat();
  }

  toggleRightBar(){
    this._OverlayService.toggleRightMenu();
  }

  public uploadFiles(event){
    this._WasmService.uploadedFile.next(event.target.files);
  }

  ChangeWasmState(i: number){
    this._WasmService.changeState.next(i);
  }

  OpenSettingsOverlay(){
    const dialogRef = this.mdialog.open(ConfigFormComponent,{
      data: Object.assign({}, this._WasmService.GetCurrentGridConfig())
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this._WasmService.SetNewGridConfigs(result);
      }
    });
  }

  RightMenuOpen(): boolean{
    return this._OverlayService.RightMenuOpen;
  }

  ActivateLayer(id: number){
    this._WasmService.activateLayer(id);
  }

  OnBrushClick(): void {
    this.ChangeWasmState(1);
    this.BrushOverlayOpen = !this.BrushOverlayOpen;
    this._WasmService.SetDrawMode();
  }

  private SendBrushUpdate(): void {
    this._WasmService.ChangeBrushSettings(this.Color, this.BrushWidth);
  }

  /**
   * Transform #R8G8B8 + A8 to A8B8G8R8
   **/
  private get Color(): number {
    const color = parseInt(this.BrushColor.slice(1, 7), 16);
    /* tslint:disable:no-bitwise */
    const red = (0xFF0000 & color) >>> 0 >> 16;
    const green = (0x00FF00 & color) >>> 0 >> 8;
    const blue = (0x0000FF & color) >>> 0;
    let sdlColor = ((red) + (green << 8) + (blue << 16)) >>> 0;
    sdlColor += (this.BrushAlpha << 24);
    /* tslint:enable:no-bitwise */
    console.log((sdlColor).toString(16));
    return sdlColor;
  }

  public ContextMenu({ x, y, ctx }: { x: number; y: number; ctx: any; }): void {
    console.log({x, y, ctx});
    this.CloseContextMenu();
    const positionStrategy = this.overlay.position()
    .flexibleConnectedTo({x, y})
    .withPositions([
      {
        originX: 'end',
        originY: 'bottom',
        overlayX: 'end',
        overlayY: 'top'
      }
    ]);

    this.overlayRef = this.overlay.create({
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.close()
    });

    this.overlayRef.attach( new TemplatePortal( this.contextMenu, this.viewContainerRef, {
    $implicit: ctx} ) );
  }

  public CloseContextMenu(): void {
    this.ContextMenuOutsideClickSubscription?.unsubscribe();
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  public MakeToken(id: number){
    this._WasmService.MakeToken(id);
  }

  public ChangeLayer(roId: number, layerId: number){
    this._WasmService.ChangeRoLayer(roId, layerId);
  }

  public RemoveRo(id: number){
    this._WasmService.RemoveRo(id);
    this.CloseContextMenu();
  }

  public isDM(): boolean {
    try {
      return this.users.isDM();
    } catch (err) {
      return false;
    }
  }

  public ChangeToSelectionMode() {
    this._WasmService.SetSelectMode();
    this.ChangeWasmState(2);
    this.BrushOverlayOpen = false;
  }

  public GetActiveMode(): MouseMode {
    return this._WasmService.GetMode();
  }

  public IsSelectionMode(): boolean {
    return this.GetActiveMode() == MouseMode.SelectionMode;
  }

  public ChangeToDrawMode():void {
    this.ChangeWasmState(1);
    this.BrushOverlayOpen = true;
    this._WasmService.SetDrawMode();
  }

}
