import { OverlayModule } from '@angular/cdk/overlay';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

import { SideButtonsComponent } from './side-buttons.component';

describe('SideButtonsComponent', () => {
  let component: SideButtonsComponent;
  let fixture: ComponentFixture<SideButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SideButtonsComponent,
      ],
      imports: [
        MatDialogModule,
        OverlayModule,
        MatMenuModule,
      ],
      providers: [
        { provide: ActivatedRoute, useValue: {} },
        { provide: UserService, useValue: {} }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
