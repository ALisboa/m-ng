import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MessageType, BaseMessage } from './../common';
import { DefaultProfileImage } from '../../common/common-types'
var _playerTest = { name: "test", icon: null  };
var _contentTest = { content: ' Hello ' };
var test = { _type: 'TEXT', player: _playerTest, payload: _contentTest };

enum ViewType {
  Default, Chat, Roll, System
}

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  placeholderImage = DefaultProfileImage;
  @Input('message') message: any;
  image: any = null;
  player: any = null;
  viewType: ViewType = ViewType.Default;
  content: any = null;

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    if(this.message){
      this.message.messageType;
      switch(this.message.messageType){
        case MessageType.CHAT_MESSAGE:{
          this.viewType = ViewType.Chat;
          this.content = this.message.content.message;
          break;
        }
        case MessageType.ROLL_MESSAGE:{
          this.viewType = ViewType.Roll;
          this.content = this.message.content.roll;
          break;
        }
        case MessageType.SYSTEM_MESSAGE:{
          this.viewType = ViewType.System;
          break;
        }
        default:{
          console.error("Unknow message Type");
          console.error(this.message.messageType);
          break;
        }
      }
    }
  }

  getImage() {

    return this.sanitizer.bypassSecurityTrustResourceUrl( (this.message.player && this.message.player.image)? this.message.player.image : this.placeholderImage );
  }

}
