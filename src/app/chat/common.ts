enum MessageType {
    SYSTEM_MESSAGE,
    ROLL_MESSAGE,
    CHAT_MESSAGE
}

interface BaseMessage {
    messageType: MessageType,
    content: SystemMessage | ChatMessage | RollMessage | string,
}

interface ChatMessage {
    player: any,
    message: string,
}

interface SystemMessage {
    error: string,
    input: string,
}

interface RollMessage {
    player: any,
    formula: string,
    roll: any,
    solved: boolean,
}

const ERROR_FORMULA = " La formula introducida no pertenece a la notación estandar. "

export { MessageType, BaseMessage, ChatMessage, RollMessage, ERROR_FORMULA, SystemMessage };
