import { TestBed } from '@angular/core/testing';

import { ChatParserService } from './chat-parser.service';

describe('ChatParserService', () => {
  let service: ChatParserService;
  const _Comment = ' Hello World ';
  const _badroll = '/r 8dd3';
  const _goodRoll = '/r 1d20+5';

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatParserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should not be roll', () => {
    expect(service.isRoll(_Comment)).toBeFalsy();
  });

  it('Should be Roll', () => {
    expect(service.isRoll(_badroll)).toBeTruthy();
    expect(service.isRoll(_goodRoll)).toBeTruthy();
  });

  it('Validate Rolls', () => {
    let formula = _badroll.slice(3);
    expect(service.isValidNotation(formula)).toBeFalsy();
    expect(service.isValidNotation(_goodRoll.slice(3))).toBeTruthy();
  })

  it('Roll Rolls', () => {
    let formula = _goodRoll.slice(3);
    let result = service.Roll(formula);
    console.debug(result);
    expect(result).toBeTruthy();
  })

});
