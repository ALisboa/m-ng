import { Injectable } from '@angular/core';
import * as  droll from 'droll';
import { BaseMessage, MessageType, RollMessage, ERROR_FORMULA, ChatMessage, SystemMessage, } from './../common';

@Injectable({
  providedIn: 'root'
})
export class ChatParserService {

  constructor() { }

  isRoll( input: string ): boolean {
    return input.startsWith('/r ');
  }

  isValidNotation( input: string ) {
    return droll.validate( input );
  }

  Roll( formula: string ) {
    return droll.roll(formula);
  }

  MakeMessage(input: string): BaseMessage {
    input = input.trim();
    let msg: BaseMessage = { messageType: MessageType.SYSTEM_MESSAGE, content: 'placeholder' };
    if(this.isRoll(input)){
      let formula = this.ObtainFormula(input);
      if(this.isValidNotation(formula)){
        let roll = this.Roll(formula);
        let rmsg: RollMessage = { player: {}, formula: formula, roll: roll, solved: true };
        msg.messageType = MessageType.ROLL_MESSAGE;
        msg.content = rmsg;
      } else {
        let smsg: SystemMessage = { error: ERROR_FORMULA, input: input };
        msg.content = smsg;
      }
    } else {
      let content: ChatMessage = { player: {}, message: input };
      msg.content = content;
      msg.messageType = MessageType.CHAT_MESSAGE;
    }
    return msg;
  }

  ObtainFormula( input: string ) {
    return input.slice(3);
  }
}
