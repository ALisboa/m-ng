import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ChatServiceService } from '../services/chat/chat-service.service';
import { PlayersService } from '../services/players/players.service';
import { ChatParserService } from './chat-parser/chat-parser.service';
import { BaseMessage, MessageType } from './common';

@Component({
  selector: 'chat-view',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {


  constructor(
    private _ChatParser: ChatParserService,
    private _ChatService: ChatServiceService,
    private _PlayerService: PlayersService,
  ) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void  {
  }

  newMessage( input: string ) {
    let msg = this._ChatParser.MakeMessage(input);
    this._ChatService.NewMessage(msg);
  }

  Messages() {
    return this._ChatService.messages ?? [];
  }

  pushMessage( msg: BaseMessage ): void {
    this._ChatService.sendMessage( msg );
  }

  submitTextArea(event: any): void {
    this.newMessage(event.target.value);
    event.target.value = "";
  }

  Roles() {
    return this._PlayerService.Characters();
  }

  CurrentRol() {
    return this._PlayerService.currentRol.name ?? 'Player';
  }

  NewRol(rol) {
    this._PlayerService.ChangeRol(rol);
  }

}
