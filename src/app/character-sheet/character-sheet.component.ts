import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChatParserService } from '../chat/chat-parser/chat-parser.service';
import { CharactersheetService } from '../services/charactersheet-service/charactersheet.service';
import { ChatServiceService } from '../services/chat/chat-service.service';
import { CharacterSheet } from './../common/common-types';

@Component({
  selector: 'app-character-sheet',
  templateUrl: './external-sheet.html',
  styleUrls: ['./external-sheet.scss']
})
export class CharacterSheetComponent implements OnInit {

  model: CharacterSheet;

  constructor(
    private _csService: CharactersheetService,
    private _chatParser: ChatParserService,
    private _chatService: ChatServiceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.model = data ?? new CharacterSheet();
  }

  ngOnInit(): void {
  }

  testOnClick(): void {
    console.log( 'Test' );
  }

  Roll(formula: string, mods: Array<string>): void {
    let mod = 0;
    mods.forEach((v) => {
      if(this.model[v]) {
        mod += Number.parseInt(this.model[v], 10);
      }
    })
    let msg = this._chatParser.MakeMessage(`/r ${formula}+${mod}`);
    this._chatService.NewMessage(msg);
  }

  _roll(mod: string) {
    this.Roll('1d20', [mod]);
  }

  _rollSave(mod: string):void  {
    if(this.model[mod+'Chk']){
      this.Roll('1d20', [mod, 'proficiency']);
    } else {
      this.Roll('1d20', [mod]);
    }
  }

  SendUpdate(): void {
    this._csService.SendUpdate(this.model);
  }

}


