import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {Category, Texture} from 'src/app/common/Category';
import { RenameTextureComponent } from 'src/app/modals/rename-texture/rename-texture.component';
import {DmEditorService} from 'src/app/services/dm-editor/dm-editor.service';

@Component({
  selector: 'dm-editor',
  templateUrl: './dm-editor.component.html',
  styleUrls: ['./dm-editor.component.scss']
})
export class DmEditorComponent implements OnInit {

  constructor(
    public dme: DmEditorService,
    private dialogService: MatDialog,
  ) {

  }

  ngOnInit() {

  }

  ShowCat(cat: Category){
    this.dme.activeCat = cat;
  }

  NewTexture(t: Texture){
    this.dme.activeCat.items.push(t);
  }

  imgStartDrag(event: any, t: Texture){
    event.dataTransfer.setData("text/json",
                               "{ \"et\":\"texture\", \"id\":" + t.id + "}");
  }

  editName(t: Texture) {
    const dialogRef = this.dialogService.open(RenameTextureComponent, {
      data: t.name,
    })

    dialogRef.afterClosed().subscribe((res) => {
      if(res) {
        t.name = res;
        this.dme.UpdateTexture(t);
      }
    })
  }

  deleteItem() {

  }

}
