import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmEditorComponent } from './dm-editor.component';

describe('DmEditorComponent', () => {
  let component: DmEditorComponent;
  let fixture: ComponentFixture<DmEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
