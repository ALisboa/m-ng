import { Component, Input, OnInit } from '@angular/core';
import { CharacterSheet } from 'src/app/common/common-types';
import { DefaultProfileImage } from 'src/app/common/common-types';
import * as Localization from 'src/app/common/localization';
import { CharactersheetService } from 'src/app/services/charactersheet-service/charactersheet.service';

@Component({
  selector: 'app-character-image',
  templateUrl: './character-image.component.html',
  styleUrls: ['./character-image.component.scss']
})
export class CharacterImageComponent implements OnInit {

  @Input('character') character: CharacterSheet;

  constructor(
    private _service: CharactersheetService,
  ) { }

  ngOnInit(): void {
  }

  ImageExist(): boolean {
    return (this.character && typeof this.character.image !== "undefined" && this.character.image !== null);
  }

  GetImage(): any {
    if(this.ImageExist()) {
      return this.character.image;
    } else {
      return DefaultProfileImage;
    }
  }

  UploadFile(event) {
    let file = event.target.files[0];
    let filereader = new FileReader();
    filereader.onloadend = () => {
      this.Replace(filereader.result);
    }
    filereader.readAsDataURL(file);
    //ReadFile
    //Make It Base 64
    //Send To Replace
  }

  Replace(image): any {
    if(this.character) {
      this.character.image = image;
      this._service.UpdateSheet(this.character);
    }
  }

  Localization(key): string {
    return Localization.EN_US[key] ?? key;
  }

  DeleteImage(): void {
    if(this.character){
      this.character.image = null;
      this._service.UpdateSheet(this.character);
    }
  }

  ExpandImage(): void {

  }


}
