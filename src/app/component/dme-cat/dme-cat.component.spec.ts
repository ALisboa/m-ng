import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmeCatComponent } from './dme-cat.component';

describe('DmeCatComponent', () => {
  let component: DmeCatComponent;
  let fixture: ComponentFixture<DmeCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmeCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmeCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
