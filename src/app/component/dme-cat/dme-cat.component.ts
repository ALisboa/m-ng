import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Category} from 'src/app/common/Category';

@Component({
  selector: 'dme-cat',
  templateUrl: './dme-cat.component.html',
  styleUrls: ['./dme-cat.component.scss']
})
export class DmeCatComponent implements OnInit {

	@Input() cat: Category;
	@Output() Selected: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.cat = {name : "placeholder", items: [], subCats: []};
  }

  ngOnInit() {
  }

	OnSelected(cat){
		this.Selected.emit(cat);
	}

}
