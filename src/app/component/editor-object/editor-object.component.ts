import { Component, OnInit, Input } from '@angular/core';
//import { CharacterSheet } from 'src/app/common/common-types';
//import { EObject } from 'src/app/common/eobject';

@Component({
  selector: 'editor-object',
  templateUrl: './editor-object.component.html',
  styleUrls: ['./editor-object.component.scss']
})
export class EditorObjectComponent implements OnInit {
  @Input('object') object: any;
  @Input('editMode') editMode: any = false;

  constructor() { }

  ngOnInit() {
  }

  DisplayContent(): string {
    if ( this.object )
      return this.object.rawContent;
    else
      return ``;
  }

  isCS() {
    if (this.object && typeof this.object.title === 'undefined') {
      return true;
    } else {
      return false;
    }
  }

  public GetTitle() {
    if(!this.object){
      return '';
    }
    if( this.isCS() ){
      return this.object.name ?? '';
    }
    else {
      return this.object.title ?? '';
    }
  }

  get title() {
    return this.GetTitle();
  }

  set title(value) {
    if(!this.object){
      return
    }
    if(!this.isCS()){
      this.object.title = value;
    } else {
      this.object.name = value;
    }
  }

  get content() {
    if(!this.object){
      return '';
    }
    return this.object.rawContent ?? '';
  }

  set content(value) {
    if(!this.object){
      return;
    }
    this.object.rawContent = value;
  }

  public CsImg() {
    return '';
  }

  public CsName() {
    if(typeof this.object.name === 'undefined') {
      return '';
    }else {
      return this.object.name;
    }
  }
}
