import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorObjectComponent } from './editor-object.component';

describe('EditorObjectComponent', () => {
  let component: EditorObjectComponent;
  let fixture: ComponentFixture<EditorObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
