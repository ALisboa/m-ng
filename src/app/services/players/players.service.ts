import { Injectable } from '@angular/core';
import { CharactersheetService } from '../charactersheet-service/charactersheet.service';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  currentRol: any;

  constructor(
    private characters: CharactersheetService,
    private usrS: UserService,
  ) {
    this.currentRol = this.usrS.GetUsr() ?? { name: 'NoPlayer' };
    this.usrS.currentUserChanged.subscribe((usr)=>{
      this.currentRol = usr;
    })
  }

  public Characters() {
    let roles = [this.currentRol];
    for( let cs of this.characters.characters ) {
      roles.push(cs);
    }
    return roles;
  }

  ChangeRol(rol) {
    this.currentRol = rol;
  }

}
