import { Injectable } from '@angular/core';
import { EObject } from 'src/app/common/eobject';
import { environment } from 'src/environments/environment';
import { CharactersheetService } from '../charactersheet-service/charactersheet.service';
import { SynkrService, MessageAction } from '../synkr/synkr.service';
import { UUID4 } from 'src/app/common/Utils';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  content =  []

  private Obs: Array<EObject> = [
    {
      id: 1,
      title: "OBJ1",
      rawContent: `
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut rutrum ligula, sed volutpat turpis. Mauris quis nibh est. Sed eu massa laoreet, commodo purus id, pellentesque ex. Etiam pretium felis lacinia, fringilla libero et, porta lorem. Donec cursus accumsan quam. Nulla vel eleifend tortor. Nunc cursus venenatis enim vitae scelerisque. Suspendisse vulputate facilisis neque a varius. Vivamus tristique non eros ac rutrum. Duis placerat est nisl, nec cursus purus bibendum ac. Sed dapibus ultricies lacus eu dignissim.
</p>
<p>
Sed eu orci consequat, consequat justo ullamcorper, finibus lacus. Praesent mollis nisi luctus, pulvinar nunc quis, hendrerit erat. Ut in pellentesque felis. Maecenas dui leo, varius non augue congue, interdum ultrices lectus. Pellentesque ornare nunc id faucibus cursus. In a velit vitae enim placerat tincidunt sed eget orci. Etiam eu quam eget odio rutrum maximus. Curabitur tempor nunc gravida neque fringilla, at facilisis lectus iaculis.
</p>
<p>
Suspendisse arcu eros, viverra eget molestie quis, auctor id nisl. Pellentesque et massa neque. Maecenas sollicitudin odio sed iaculis consequat. Morbi rhoncus ac erat id sodales. Ut metus turpis, sodales in facilisis eu, lacinia eu risus. Sed sit amet facilisis erat, a posuere orci. Fusce rutrum dolor non consequat vulputate. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras luctus in mauris at vehicula. Pellentesque massa orci, faucibus vel nibh id, vulputate pulvinar odio. Donec eget massa nisl.
</p>
Aenean lorem dolor, rhoncus ac augue et, ornare efficitur odio. Quisque justo dui, lacinia ut accumsan eu, accumsan quis lorem. Vivamus convallis nisi risus, vulputate lacinia magna vehicula eget. Donec volutpat quam et laoreet vehicula. Sed luctus, lorem nec luctus suscipit, ex dui consequat nibh, a cursus nisi dui sit amet ex. Donec lobortis vestibulum consectetur. Pellentesque euismod porttitor sem, sed tempus massa ultrices vel. Aenean vel ultricies sapien, vehicula commodo nibh. Aenean id leo sed eros vestibulum sollicitudin. Quisque ipsum erat, viverra et laoreet at, sagittis vitae justo. Integer vestibulum ornare sapien, vel pretium elit sodales non. Donec luctus justo nisi, in varius neque elementum in.

<p>Aliquam nec dui dolor. Vivamus sed pellentesque ex. Nulla consectetur tempus risus, ac rutrum felis eleifend non. Sed eu tempus arcu. Ut hendrerit dui lectus, ut cursus lorem gravida eu. Sed accumsan ultrices consequat. Vivamus non semper tellus, id facilisis dolor. Etiam pellentesque aliquet magna eu bibendum. In dapibus vel nunc sit amet dictum. Nulla facilisi. Vivamus sodales felis et turpis pulvinar placerat. Aliquam ut rutrum tellus. Etiam venenatis augue vel metus tincidunt, eget euismod sapien accumsan.</p>

<p>Quisque dapibus, sapien eu varius gravida, augue dolor volutpat magna, nec vehicula lorem sapien a urna. Aenean at aliquam nibh, ut finibus tortor. Nullam vulputate condimentum massa ac eleifend. Vivamus finibus ex nec lectus auctor gravida. Curabitur arcu ex, fringilla sed condimentum ac, pharetra et lectus. Nulla id nisi urna. Pellentesque ex magna, congue eu euismod ut, sodales non nisi. Proin aliquet elementum tortor, accumsan vestibulum ante porta ut. Etiam ligula nunc, placerat ut tortor sit amet, sodales accumsan metus.</p>

<p>Donec pretium sem sit amet vestibulum vestibulum. Donec in semper enim, quis viverra turpis. Maecenas fringilla sapien arcu, sed suscipit orci lobortis eget. Morbi interdum ut neque at mattis. Nullam consequat vitae justo eget fermentum. Aenean lobortis, libero a maximus suscipit, urna dui congue orci, in suscipit velit diam dictum massa. Quisque convallis justo auctor, posuere elit in, tempor dolor. Nulla quis nisi varius, fermentum dui ut, feugiat mauris. Curabitur lectus enim, consequat nec sodales eu, dictum eget nunc. Phasellus eu enim ac neque tincidunt gravida non at quam. Fusce rhoncus finibus lacus, et suscipit eros suscipit eu. In id magna sit amet tortor faucibus feugiat. Sed lobortis rutrum justo, eu varius quam dapibus sit amet. Nullam lacinia libero a turpis varius, egestas tempor sapien ultricies. Cras posuere nisi purus, nec finibus metus mattis ac. Quisque interdum risus vitae nisi elementum rhoncus.</p>

<p>Nulla eros tellus, gravida in lacus vitae, vestibulum rhoncus justo. Proin non turpis tortor. Ut molestie quam magna, non semper libero fringilla sit amet. Quisque in nisi urna. Vivamus sed rhoncus orci. Praesent dictum diam sit amet eleifend sollicitudin. Fusce finibus sem enim, vitae commodo mauris convallis eget. Donec velit ante, tempor vel lacus eu, pharetra sodales urna. Nullam ullamcorper congue metus sit amet faucibus. Nam sit amet arcu sed augue dignissim maximus et a felis. Suspendisse sodales posuere iaculis. Sed mauris dolor, mattis ac magna sit amet, auctor interdum sapien. Mauris varius ipsum nibh, quis cursus lacus feugiat ut.</p>
</div>
`
},
    {
      id: 2,
      title: "OBJ2",
      rawContent: `
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut rutrum ligula, sed volutpat turpis. Mauris quis nibh est. Sed eu massa laoreet, commodo purus id, pellentesque ex. Etiam pretium felis lacinia, fringilla libero et, porta lorem. Donec cursus accumsan quam. Nulla vel eleifend tortor. Nunc cursus venenatis enim vitae scelerisque. Suspendisse vulputate facilisis neque a varius. Vivamus tristique non eros ac rutrum. Duis placerat est nisl, nec cursus purus bibendum ac. Sed dapibus ultricies lacus eu dignissim.
</p>
<p>
Sed eu orci consequat, consequat justo ullamcorper, finibus lacus. Praesent mollis nisi luctus, pulvinar nunc quis, hendrerit erat. Ut in pellentesque felis. Maecenas dui leo, varius non augue congue, interdum ultrices lectus. Pellentesque ornare nunc id faucibus cursus. In a velit vitae enim placerat tincidunt sed eget orci. Etiam eu quam eget odio rutrum maximus. Curabitur tempor nunc gravida neque fringilla, at facilisis lectus iaculis.
</p>
<p>
Suspendisse arcu eros, viverra eget molestie quis, auctor id nisl. Pellentesque et massa neque. Maecenas sollicitudin odio sed iaculis consequat. Morbi rhoncus ac erat id sodales. Ut metus turpis, sodales in facilisis eu, lacinia eu risus. Sed sit amet facilisis erat, a posuere orci. Fusce rutrum dolor non consequat vulputate. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras luctus in mauris at vehicula. Pellentesque massa orci, faucibus vel nibh id, vulputate pulvinar odio. Donec eget massa nisl.
</p>
Aenean lorem dolor, rhoncus ac augue et, ornare efficitur odio. Quisque justo dui, lacinia ut accumsan eu, accumsan quis lorem. Vivamus convallis nisi risus, vulputate lacinia magna vehicula eget. Donec volutpat quam et laoreet vehicula. Sed luctus, lorem nec luctus suscipit, ex dui consequat nibh, a cursus nisi dui sit amet ex. Donec lobortis vestibulum consectetur. Pellentesque euismod porttitor sem, sed tempus massa ultrices vel. Aenean vel ultricies sapien, vehicula commodo nibh. Aenean id leo sed eros vestibulum sollicitudin. Quisque ipsum erat, viverra et laoreet at, sagittis vitae justo. Integer vestibulum ornare sapien, vel pretium elit sodales non. Donec luctus justo nisi, in varius neque elementum in.

<p>Aliquam nec dui dolor. Vivamus sed pellentesque ex. Nulla consectetur tempus risus, ac rutrum felis eleifend non. Sed eu tempus arcu. Ut hendrerit dui lectus, ut cursus lorem gravida eu. Sed accumsan ultrices consequat. Vivamus non semper tellus, id facilisis dolor. Etiam pellentesque aliquet magna eu bibendum. In dapibus vel nunc sit amet dictum. Nulla facilisi. Vivamus sodales felis et turpis pulvinar placerat. Aliquam ut rutrum tellus. Etiam venenatis augue vel metus tincidunt, eget euismod sapien accumsan.</p>

<p>Quisque dapibus, sapien eu varius gravida, augue dolor volutpat magna, nec vehicula lorem sapien a urna. Aenean at aliquam nibh, ut finibus tortor. Nullam vulputate condimentum massa ac eleifend. Vivamus finibus ex nec lectus auctor gravida. Curabitur arcu ex, fringilla sed condimentum ac, pharetra et lectus. Nulla id nisi urna. Pellentesque ex magna, congue eu euismod ut, sodales non nisi. Proin aliquet elementum tortor, accumsan vestibulum ante porta ut. Etiam ligula nunc, placerat ut tortor sit amet, sodales accumsan metus.</p>

<p>Donec pretium sem sit amet vestibulum vestibulum. Donec in semper enim, quis viverra turpis. Maecenas fringilla sapien arcu, sed suscipit orci lobortis eget. Morbi interdum ut neque at mattis. Nullam consequat vitae justo eget fermentum. Aenean lobortis, libero a maximus suscipit, urna dui congue orci, in suscipit velit diam dictum massa. Quisque convallis justo auctor, posuere elit in, tempor dolor. Nulla quis nisi varius, fermentum dui ut, feugiat mauris. Curabitur lectus enim, consequat nec sodales eu, dictum eget nunc. Phasellus eu enim ac neque tincidunt gravida non at quam. Fusce rhoncus finibus lacus, et suscipit eros suscipit eu. In id magna sit amet tortor faucibus feugiat. Sed lobortis rutrum justo, eu varius quam dapibus sit amet. Nullam lacinia libero a turpis varius, egestas tempor sapien ultricies. Cras posuere nisi purus, nec finibus metus mattis ac. Quisque interdum risus vitae nisi elementum rhoncus.</p>

<p>Nulla eros tellus, gravida in lacus vitae, vestibulum rhoncus justo. Proin non turpis tortor. Ut molestie quam magna, non semper libero fringilla sit amet. Quisque in nisi urna. Vivamus sed rhoncus orci. Praesent dictum diam sit amet eleifend sollicitudin. Fusce finibus sem enim, vitae commodo mauris convallis eget. Donec velit ante, tempor vel lacus eu, pharetra sodales urna. Nullam ullamcorper congue metus sit amet faucibus. Nam sit amet arcu sed augue dignissim maximus et a felis. Suspendisse sodales posuere iaculis. Sed mauris dolor, mattis ac magna sit amet, auctor interdum sapien. Mauris varius ipsum nibh, quis cursus lacus feugiat ut.</p>
</div>
`},
    {
  id: 3,
  title: "Goblin",
  rawContent:`
<em>Small humanoid (goblinoid), neutral evil</em>
<hr>
<p><b>Armor Class</b> 15 (Leather Armor, Shield)</p>
<p><b>Hit Points</b> 7 (2d6)</p>
<p><b>Speed</b>30ft.</p>
<hr>
<p><b>STR</b> 8  (-1)</p>
<p><b>DEX</b> 14 (+2)</p>
<p><b>CON</b> 10 (+0)</p>
<p><b>INT</b> 10 (+0)</p>
<p><b>WIS</b> 8  (-1)</p>
<p><b>CHA</b> 8  (-1)</p>
<hr>
<p><b>Skills</b> Stealh +6</p>
<p><b>Senses</b> Darkvision 60ft., passive Perception 9</p>
<p><b>Languages</b> Common, Goblin</p>
<p><b>Challenge</b> 1/4 (50 XP)</p>
<hr>
<p><b> Nimble Escape.</b> The goblin can take Disengage or Hide action as a bonus action on each of its turns.</p>
<br>
<h2>Actions</h2>
<hr>
<p><b>Scimitar.</b> <em> Melee Weapon Attack:</em> +4 to hit, reach 5 ft., one target. <em>Hit:</em> (1d6 + 2) slashing damage.</p>
<p><b>Shortbow.</b> <em> Ranged Weapon Attack:</em> +4 to hit, reach 80/320 ft., one target. <em>Hit:</em> (1d6 + 2) piercing damage.</p>
`
}
  ]
  private updatedObject: Subscription;
  private removeObject: Subscription;

  constructor(
    private _characters: CharactersheetService,
    private _Synkr: SynkrService,
  ) {
    this.updatedObject = this._Synkr.updateContent.subscribe(
      (data)=>{
        this.ContentUpdate(JSON.parse(data.payload));
      }
    );
    this.removeObject = this._Synkr.removeContent.subscribe(
      (data)=>{
        this.RemovedContent(JSON.parse(data.payload));
      }
    );
  }

  public newPage(): boolean {
    let id = UUID4();
    let obj = new EObject();
    obj.id = id;
    obj.title = 'New Object';
    obj.rawContent = '<p> placeholder </p>';
    this.content.push(obj);
    this.UpdateContent(obj);
    return true;
  }

  public UpdateContent(obj) {
    let i = this.Exist(obj.id);
    if(i !== -1) {
      this._Synkr.sendMessage({ id: '0', action: MessageAction.UPDATE_CONTENT, payload: JSON.stringify(obj) });
    }
  }

  public ContentUpdate(obj) {
    let i = this.Exist(obj.id);
    if(i!==-1){
      this.content[i].title = obj.title;
      this.content[i].rawContent= obj.rawContent;
    }else{
      this.content.push(obj);
    }
  }

  public RemovedContent(obj){
    let i = this.Exist(obj.id);
    if(i!==-1){
      this.content.splice(i, 1);
    }
  }

  public RemoveContent(obj){
    this.RemovedContent(obj);
    let msg = {action: MessageAction.REMOVE_CONTENT, id: '0', payload: JSON.stringify(obj)};
    this._Synkr.sendMessage(msg);
  }

  public Exist(id): number {
    for(var i = 0; i < this.content.length; ++i){
      let local = this.content[i];
      if(local.id===id){
        return i;
      }
    }
    return -1;
  }

}
