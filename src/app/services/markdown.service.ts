import { Injectable } from '@angular/core';
import * as MarkdownIt from 'markdown-it';
import * as Token from 'markdown-it/lib/token';
import * as Trie from './../common/trie';

@Injectable({
  providedIn: 'root'
})
export class MarkdownService {

  MD = new MarkdownIt();
  NodeIndex: Map<string, TokenTreeNode> = new Map();

  constructor() {

  }

  GenerateKeywords(input: any){

  }

  GenerateTitles(input: Token[]){
    let titles = new Array<string>();
    console.log(input);
    for(var tkn = 0; tkn < input.length; ++tkn){
      if( input[tkn].type === "heading_open" ){
        titles.push( input[++tkn].content );
        ++tkn; // Advancing into the heading_close token
      }
    }
    return titles;
  }

  GenerateTokenTrees(tokens: Token[]): TokenTreeNode[] {
    let lvl0 = new TokenTreeNode();
    lvl0.Parent = null;
    lvl0.title = "";
    lvl0.markup = "";
    let parents = [lvl0];
    let nodes = [lvl0];
    for(var tkn = 0; tkn < tokens.length; ++tkn){
      if(tokens[tkn].type === 'heading_open'){
        // new header
        let node = new TokenTreeNode();
        node.title = tokens[tkn + 1].content;
        node.markup = tokens[tkn].markup;
        while ( node.markup.length <= parents[parents.length - 1].markup.length ){
          // TODO: Assert in case of wierd behavior of '' heading
          parents.pop()
        }
        node.Parent = parents[parents.length - 1];
        parents.push(node);
        nodes.push(node);
      }
      for(var parent of parents){
        parent.children.push(tokens[tkn]);
      }
    }
    return nodes;
  }

  ParseMD(input: string){
    let tokens = this.MD.parse(input, []);
    let nodes = this.GenerateTokenTrees(tokens);
    for ( var node of nodes ){
      this.NodeIndex.set(node.title, node);
    }
  }

  RenderMD(input: string){

  }

  GenerateSearchTree(contentNodes: TokenTreeNode[]): Trie.TrieNode {
    let stree = new Trie.TrieNode();
    stree.children = new Map();
    for(var node of contentNodes){
      Trie.Insert(node.title, node, stree);
    }
    return stree;
  }

}

class TokenTreeNode{
  Parent: TokenTreeNode | null;
  children: Array<Token> = [];
  title: string;
  markup: string;

  TokenTreeNode(){
  }
}
