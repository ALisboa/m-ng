import { Injectable } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SynkrService} from 'src/app/services/synkr/synkr.service';
import { UserDB } from 'src/app/common/Utils.js';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	currentUser: User;
	currentRoom: string;
	usrs: Map<number, User> = new Map();
	currentUserChanged: Subject<any> = new Subject();

	constructor(private route : ActivatedRoute, private synker:SynkrService
			   ) {
		this.route.queryParamMap.subscribe((params) => {
			let r = params.get('room');
			if (!r){
				r = Math.round(Math.random()*500).toString();
			}
			if(!this.currentUser) {
				this.RetrieveUser();
			}
			this.currentRoom = r;
		})
	}

	public isDM(): boolean{
		if(this.currentUser && this.currentUser.Rol === UserRol.DM){
			return true;
		}
		return false;
	}

	Login(username: string, password: string): any{
		//TODO: Service Login
		for(var usr of UserDB){
			if( usr.username === username && usr.password === password ){
				return this.SetUser(usr);
			}
		}
		return false;
	}

	SetUser(usr: any): boolean {
		let user: User = {name: usr.username, Rol: usr.Rol};
		this.usrs.set(usr.id, user);
		this.currentUser = user;
		this.StoreUser();
		this.currentUserChanged.next(this.currentUser);
		return true;
	}

	StoreUser(): void {
		localStorage.setItem('usr', JSON.stringify(this.currentUser));
	}

	RetrieveUser(): void {
		let usr = localStorage.getItem('usr');
		if(usr) {
			this.currentUser = JSON.parse(usr);
		}
	}

	GetUsr(): User | undefined {
		if(!this.currentUser) {
			this.RetrieveUser();
		}
		return this.currentUser;
	}
}

interface User {
  name: string;
  Rol: UserRol;
}

enum UserRol{
	DM, User
}
