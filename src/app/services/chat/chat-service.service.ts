import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { ChatParserService } from 'src/app/chat/chat-parser/chat-parser.service';
import { BaseMessage, MessageType } from 'src/app/chat/common';
import { PlayersService } from '../players/players.service';
import { SynkrService, MessageAction } from '../synkr/synkr.service';

@Injectable({
  providedIn: 'root'
})
export class ChatServiceService {

  messages: Array<BaseMessage> = new Array();

  public recievedMessage: Subject<any>;
  private _recievedMessage: Subscription;

  constructor(
    private parserService: ChatParserService,
    private synkr: SynkrService,
    private playerService: PlayersService,
  ) {
    this.recievedMessage = new Subject();
    this._recievedMessage= this.synkr.recievedMessage.subscribe((message) => {
      this.gotMessage(JSON.parse(message.payload));
    })
  }

  public sendMessage(msg: BaseMessage) {
    let payload = JSON.stringify(msg);
    let data = {action: MessageAction.MESSAGE, payload: payload, id: '0' };
    this.synkr.sendMessage(data);
  }

  private gotMessage(msg) {
    this.messages.push(msg);
  }

  public NewMessage(msg: BaseMessage) {
    msg['player'] = this.playerService.currentRol;
    this.messages.push(msg);
    switch(msg.messageType) {
        case MessageType.SYSTEM_MESSAGE: {
          console.log(msg);
          break;
        }
        default: {
          this.sendMessage(msg);
        }
    }
  }
}
