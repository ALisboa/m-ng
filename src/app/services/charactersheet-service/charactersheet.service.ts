import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageAction, SynkrService } from '../synkr/synkr.service';
import { CharacterSheet } from './../../common/common-types';
import { UUID4 } from './../../common/Utils';

@Injectable({
  providedIn: 'root'
})
export class CharactersheetService {

  characters: Array<CharacterSheet> = new Array();
  characterUpdated: Subscription;
  characterDeleted: Subscription;

  constructor(
    private _Synk: SynkrService,
  ) {
    this.characterUpdated = this._Synk.updateCS.subscribe((msg) => {
      this.UpdatedSheet(JSON.parse(msg.payload));
    });
    this.characterDeleted = this._Synk.removeCS.subscribe((msg) => {
      this.DeletedCS(JSON.parse(msg.payload));
    })
  }

  SendUpdate(sheet: CharacterSheet) {
    this._Synk.sendMessage( {action: MessageAction.UPDATE_CS, payload: JSON.stringify(sheet), id:''} );
  }

  DeleteCS(sheet: CharacterSheet) {
    this._Synk.sendMessage( { action: MessageAction.REMOVE_CS, payload: JSON.stringify(sheet), id:'' });
    this.DeletedCS( sheet );
  }

  DeletedCS( sheet: CharacterSheet ) {
    for (let i = 0; i < this.characters.length; ++i ){
      if (this.characters[i].id === sheet.id) {
        this.characters.splice(i, 1);
        return;
      }
    }
  }

  CreateSheet(): void {
    let cs = new CharacterSheet();
    cs.id = UUID4();
    cs.name = 'New';
    this.characters.push(cs);
    this.SendUpdate(cs);
  }

  UpdateSheet(cs: CharacterSheet): void {
    this.UpdatedSheet(cs);
    this.SendUpdate(cs);
  }

  UpdatedSheet( cs: CharacterSheet ) {
    let local = null;
    for(var c of this.characters){
      if(cs.id === c.id){
        local = c;
        break;
      }
    }
    if(local){
      const keys = Object.keys(cs);
      keys.forEach((key) => {
        local[key] = cs[key];
      });
    }
    else {
      this.characters.push(cs);
    }
  }

}
