import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DisplayPage } from 'src/app/common/DisplayPage';

@Injectable({
  providedIn: 'root'
})
export class OverlayService {
  public RightMenuOpen: boolean = false;
  public TabAction: Subject<any> = new Subject<any>();
  constructor() { }

  toggleRightMenu() {
    this.RightMenuOpen = !this.RightMenuOpen;
  }

  OpenChat(){
    this.TabAction.next(DisplayPage.Chat)
    this.RightMenuOpen = true;
  }

  OpenJournal(){
    this.TabAction.next(DisplayPage.Journal)
    this.RightMenuOpen = true;
  }

  OpenEditor(){
    this.TabAction.next(DisplayPage.Editor)
    this.RightMenuOpen = true;
  }

}
