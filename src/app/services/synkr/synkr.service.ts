/* 
 * Author: Alan Deni Lisboa Orellana
 * 2019AD
 */
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoaderComponent } from 'src/app/loader/loader.component';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class SynkrService {

    ws: WebSocket;
    wss$ = new Subject<any>();
    update = new Subject<any>();
    new_image = new Subject<any>();
    public gridconfig = new Subject<any>();
    public updateCS = new Subject<any>();
    public removeCS = new Subject<any>();
    public updateContent = new Subject<any>();
    public update_texture = new Subject<any>();
    public remove_texture = new Subject<any>();
    public update_config = new Subject<any>();
    removeContent = new Subject<any>();

    public recievedMessage = new Subject<any>();
    destination = 'ws://' + environment.DOMAIN_NAME + '/ws/game/';
    msghistory = new Array();
    private dialogref: any;

    constructor(private dialog: MatDialog, private snackbar: MatSnackBar) {
    }

    connect(game: string){
        const dest = this.destination + game + '/';
        this.ws = new WebSocket(dest);
        this.ws.onmessage = ( msg ) => {
            this.processMessage(msg);
        };
        this.ws.onopen = () => {
            this.getBacklog();
        };
        this.ws.onerror = (event) => {
            this.snackbar.open("Error connecting to socket. Trying again.")
            console.error(event);
            setTimeout(()=>{
                this.connect(game);
            }, 5000)
        };
    }

    public InitLoadingDialog() {
        this.dialogref = this.dialog.open(LoaderComponent, {closeOnNavigation: false, disableClose: true, backdropClass: 'loading-backdrop'});
    }

    sendMessage(msg: SynkerMessage){
        if (msg.id === '0'){
            msg.id = UUID4();
        }
        this.msghistory.push(msg.id);
        if (this.msghistory.length > 70){
            this.msghistory.shift();
        }
        this.ws?.send(JSON.stringify(msg));
    }

    getBacklog(){
        this.ws?.send(JSON.stringify({'action': 'request'}))
    }

    processMessage( msg: any ){
        const data = JSON.parse(msg.data?? msg);
        if (Array.isArray(data)){
            for( var message of data ) {
                this.processMessage(message);
            }
            setTimeout(()=>this.dialogref.close(), 5000);
        }
        if (!this.hasID(data)){
            switch(data.action) {
                case(MessageAction.UPDATE): {
                    this.update.next({payload: data.payload});
                    return;
                }
                case(MessageAction.NEW_IMAGE): {
                    this.new_image.next({payload: data.payload});
                    return;
                }
                case(MessageAction.MESSAGE): {
                    this.recievedMessage.next({payload: data.payload});
                    return;
                }
                case(MessageAction.UPDATE_CS): {
                    this.updateCS.next({payload: data.payload});
                    return;
                }
                case(MessageAction.REMOVE_CS): {
                    this.removeCS.next({payload: data.payload});
                    return;
                }
                case(MessageAction.UPDATE_CONTENT): {
                    this.updateContent.next({payload: data.payload});
                    return;
                }
                case(MessageAction.REMOVE_CONTENT): {
                    this.removeContent.next({payload: data.payload});
                    return;
                }
                case(MessageAction.UPDATE_TEXTURE): {
                    this.update_texture.next({payload: data.payload});
                    return;
                }
                case(MessageAction.REMOVE_TEXTURE): {
                    this.remove_texture.next({payload: data.payload});
                    return;
                }
                case(MessageAction.UPDATE_CONFIG): {
                    this.update_config.next({payload: data.payload});
                    return;
                }
                default: {
                    this.wss$.next(data);
                    return
                }
            }
        }
    }

    hasID(msg: SynkerMessage): boolean{
        for(var i = 0; i < this.msghistory.length; i++){
            if(this.msghistory[i] === msg.id){
                return true;
            }
        }
        return false;
    }
}

interface SynkerMessage {
    action: MessageAction; payload: any; id: string;
}

export enum MessageAction {
    UPDATE,
    MESSAGE,
    NEW_IMAGE,
    REMOVE_CONTENT,
    UPDATE_CONTENT,
    REMOVE_CS,
    UPDATE_CS,
    UPDATE_TEXTURE,
    REMOVE_TEXTURE,
    UPDATE_CONFIG,
}

// BY Kaizhu256, https://gist.github.com/kaizhu256/4482069

function UUID4() {
    //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    let uuid = '';
    let ii;
    for (ii = 0; ii < 32; ii += 1) {
        switch (ii) {
            case 8:
            case 20:
                uuid += '-';
                uuid += (Math.random() * 16 | 0).toString(16);
                break;
            case 12:
                uuid += '-';
                uuid += '4';
                break;
            case 16:
                uuid += '-';
                uuid += (Math.random() * 4 | 8).toString(16);
                break;
            default:
                uuid += (Math.random() * 16 | 0).toString(16);
        }
    }
    return uuid;
}
