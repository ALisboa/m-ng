import { TestBed } from '@angular/core/testing';

import { SynkrService } from './synkr.service';

describe('SynkrService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SynkrService = TestBed.get(SynkrService);
    expect(service).toBeTruthy();
  });
});
