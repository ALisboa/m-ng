import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { GridConfigs } from 'src/app/common/GridConfigs';
import { DisplayComponent } from 'src/app/grid/display/display.component';
import { MessageAction, SynkrService } from '../synkr/synkr.service';

enum MouseMode {
  SelectionMode, DrawMode
};

@Injectable({
  providedIn: 'root'
})
export class WasmService {
  public uploadedFile = new Subject();
  public changeState = new Subject();
  public layerChanged = new Subject();
  public BrushSettingChanged = new Subject();
  public RightClickCalled = new Subject();
  public $ChangeRolayer = new Subject();
  public $MakeToken = new Subject();
  public $RemoveRo = new Subject();
  public $CopyRoToCP = new Subject();
  public $PasteRo = new Subject();
  public CopyRo = new Subject();
  public layers: Array<ILayer>;
  public ActiveMode = MouseMode.SelectionMode;
  WasmRef: DisplayComponent;
  private synk_sub: Subscription;

  constructor(private synkr: SynkrService) {
    //TODO: READ FROM WASM OR FILE
    this.layers = [
      {name:"Background",
       id: 1,
       layertype: LayerType.Overlay
      },
      {
        name:"Token",
        id:2,
        layertype:LayerType.Token
      },
      {
        name:"Overlay",
        id:3,
        layertype:LayerType.Overlay
      },
      {
        name:"Light",
        id:4,
        layertype: LayerType.Light
      }
    ];
    this.synk_sub = this.synkr.update_config.subscribe((payload)=>{
      this._UpdatedGridConfigs(JSON.parse(payload.payload));
    })
  }

  public UploadFile(files: FileList){
    this.uploadedFile.next(files);
  }

  public SetComponent(wasm: DisplayComponent){
    this.WasmRef = wasm;
  }

  public GetCurrentGridConfig(): GridConfigs{
    return this.WasmRef.getCurrentGridConfigs();
  }

  private _UpdatedGridConfigs(gcs: GridConfigs) {
    this.WasmRef.setGridConfigs(gcs);
  }

  public SetNewGridConfigs(gcs: GridConfigs){
    this.WasmRef.setGridConfigs(gcs);
    this.UpdateGridConfigs(gcs);
  }

  private UpdateGridConfigs(gcs: GridConfigs){
    this.synkr.sendMessage({action: MessageAction.UPDATE_CONFIG, id: '0', payload: JSON.stringify(gcs)});
  }

  public ChangeBrushSettings(color: number, width: number){
    this.WasmRef.ChangeBrush(color, width);
  }

  public activateLayer(id: number){
    this.layerChanged.next(id);
  }

  /**
   * For the Side buttons outline
   */
  public OpenContextMenu(x: number, y: number, ctx: any){
    this.RightClickCalled.next({x, y,  ctx});
  }

  public MakeToken(id: number){
    this.$MakeToken.next({id});
  }

  public ChangeRoLayer(id: number, layerId: number){
    this.$ChangeRolayer.next({id, layerId});
  }

  public RemoveRo(id: number){
    this.$RemoveRo.next({id});
  }

  public CopyRoToClipboard(){
    this.$CopyRoToCP.next(true);
  }

  public GetMode(): number {
    return this.ActiveMode;
  }

  public SetDrawMode(): number {
    return this.ActiveMode = MouseMode.DrawMode;
  }

  public SetSelectMode(): number {
    return this.ActiveMode = MouseMode.SelectionMode;
  }
}

enum LayerType{
  Overlay, Token, Light,
}

interface ILayer{
  id: number;
  name: string;
  layertype: LayerType;
}
