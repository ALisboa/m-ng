import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import ImageTools from '../../common/ImageTools.js'
import {DomSanitizer} from '@angular/platform-browser';
import {Category, Texture} from 'src/app/common/Category.js';
import { MessageAction, SynkrService } from '../synkr/synkr.service.js';
@Injectable({
  providedIn: 'root'
})
export class DmEditorService {
  TextureEvents: Subject<any> = new Subject<any>();
	public Cats: Array<Category>;
  public activeCat: Category;
  private _update_texture: Subscription;
  private _remove_texture: Subscription;

  constructor(
    private sanitizer: DomSanitizer,
    private Synkr: SynkrService,
	) {
		this.Cats = [{name: "Cat", items:[], subCats:[]}];
		this.activeCat = this.Cats[0];
    this._remove_texture = this.Synkr.remove_texture.subscribe((dataPayload) => {
      this.DeletedTexture(JSON.parse(dataPayload));
    } );
    this._update_texture = this.Synkr.update_texture.subscribe((dataPayload)=> {
      this.UpdatedTexture(JSON.parse(dataPayload));
    })
	}

  GeneratePreview(f:File, file:string, id: number){
    ImageTools.resize(f, {width:64, height: 64}, (blob:Blob
			, result: any) => {
			if(result){
				const text = {name: file, id: id, preview: this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob))}; 
				this.activeCat.items.push(<Texture>text);
			}		
    });
  }

  UpdatedTexture(text: Texture) {
    let pos = this._find(text.id);
    if(pos === -1) {
      this.activeCat.items.push(text);
    } else {
      let item = this.activeCat.items[pos];
      item.name = text.name
      //TODO: Update the rest of the attributes.
    }
  }

  UpdateTexture(text: Texture) {
    this.UpdatedTexture(text);
    const payload = JSON.stringify(text);
    this.Synkr.sendMessage({action: MessageAction.UPDATE_TEXTURE, id: '0', payload: payload});
  }

  DeleteTexture(text: Texture) {
    this.DeletedTexture(text);
    const payload = JSON.stringify(text);
    this.Synkr.sendMessage({action: MessageAction.REMOVE_TEXTURE, id: '0', payload: payload});
  }

  DeletedTexture(text: Texture) {
    let pos = this._find(text.id);
    if(pos !== -1) {
      this.activeCat.items.splice(pos, 1);
    }
  }

  _find(id: number): number {
    for(let i = 0; i < this.activeCat.items.length ; ++i) {
      if(this.activeCat.items[i].id === id) {
        return i;
      }
    }
    return -1;
  }

}
