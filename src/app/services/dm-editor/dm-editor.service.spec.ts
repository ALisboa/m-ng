import { TestBed } from '@angular/core/testing';

import { DmEditorService } from './dm-editor.service';

describe('DmEditorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DmEditorService = TestBed.get(DmEditorService);
    expect(service).toBeTruthy();
  });
});
