import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayerService {
	
	public layers: Array<ILayer>;

  constructor() { }
}

enum LayerType{
	Overlay, Token, Light,
}

interface ILayer{
	id: number;
	name: string;
	layertype: LayerType;
}
