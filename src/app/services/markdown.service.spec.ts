import { TestBed } from '@angular/core/testing';

import { MarkdownService } from './markdown.service';
import * as Trie from 'src/app/common/trie';

describe('MarkdownService', () => {
  let service: MarkdownService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarkdownService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make title lists', () => {
    let tokens = service.MD.parse(example, []);
    const expectedTitles = [
      "Title 1",
      "Subtitle 1",
      "subtitle 2",
      "Title 2"
    ];
    let res = service.GenerateTitles(tokens);
    console.log(service.MD.renderer.render(tokens, {}, {}))
    expect(res).toEqual(expectedTitles);
  });

  it('should generate a 4 levels tree', () => {
    let tokens = service.MD.parse(example, []);
    const resSize = 5;
    const res = service.GenerateTokenTrees(tokens);
    console.log(res);
    expect(res.length).toEqual(resSize);
  });

  it('tree children should have correct titles', () => {
    let tokens = service.MD.parse(example, []);
    const title1 = "Title 1", title2 = "Subtitle 1", title3 = "subtitle 2", title0 = "", title4 = "Title 2";
    const res = service.GenerateTokenTrees(tokens);
    console.log(res);
    expect(res[0].title).toEqual(title0);
    expect(res[1].title).toEqual(title1);
    expect(res[2].title).toEqual(title2);
    expect(res[3].title).toEqual(title3);
    expect(res[4].title).toEqual(title4);
  });

  it('Prefix Tree should build', () => {
    let tokens = service.MD.parse(example, {});
    const contentNodes = service.GenerateTokenTrees(tokens);
    const Searchtree = service.GenerateSearchTree(contentNodes);
    const res = Trie.KeysWithPrefix(Searchtree, "Tit");
    expect(res).toEqual(['Title 1', 'Title 2']);
    expect(Trie.KeysWithPrefix(Searchtree, "Sub")).toEqual(['Subtitle 1']);
  })
});

const example = `# Title 1
Content of Title 1
## Subtitle 1
content of subtitle 1
## subtitle 2
Content of subtitle 2
# Title 2
`;
