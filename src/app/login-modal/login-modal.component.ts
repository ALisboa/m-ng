import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  username: string;
  password: string;
  room: string;

  constructor(
    private usrS: UserService,
    private router: Router,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {

  }

  login(): void {
    if( this.usrS.Login(this.username, this.password) ) {
      this.router.navigate(['/game'], {queryParams: {room: 20}});
    } else {
      this.snackbar.open('Incorrect Username or Password', 'Dismiss');
    }
  }


}
